//
//  ImageAnalysis.h
//  sorghum-iOS
//

//  Created by cis on 11/27/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import <opencv2/videoio/cap_ios.h>

@interface ImageAnalysis : NSObject

//- (UIImage *) findSquaresContours: (UIImage *) imageToProcess : (int ) threshold: (NSNumber *) rectangleSize;

- (UIImage *) uniformScale: (UIImage*) image;

- (UIImage * ) analysis: (UIImage *) image : ( double * ) result;
@property (strong,nonatomic) NSMutableArray * images;

//@property (strong,nonatomic) CvVideoCamera * camera;

@end
