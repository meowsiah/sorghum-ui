//
//  ImageAnalysis.m
//  sorghum-iOS
//
//  Created by cis on 11/27/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import "ImageAnalysis.hpp"


#import <opencv2/opencv.hpp>
#include "vector"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include <queue>


#include <iostream>
#include <math.h>
#include <string.h>

using namespace::cv;
using namespace::std;

@implementation ImageAnalysis
@synthesize images;

const int minPercentContour = 5;


Mat src;
Mat src_gray;
Mat lines;
Mat canny_output;
Mat  erosion_dst, dilation_dst;

//image slider
int image = 38;
int max_imgThresh = 323;
//image slider

//canny
int thresh = 30;
int max_thresh = 255;
int minArea = 25;
int lineThreshold = 5;
///canny

//erosion
int erosion_elem = 0;
int erosion_size = 15;
int dilation_elem = 0;
int dilation_size = 0;

///erosion

//squareDetection
double lowerThreshHoldSize = 0.001;
double upperThreshHoldSize = 0.05;
int N = 3;
int squaresThresh = 50;
int squareSizePaper =1;

RNG rng(12345);

//fileIO
priority_queue< double> sortSizes;

//const String originalWindowName = "Canny";
const String contourWindowName = "Contours";

vector<vector<cv::Point> > contours;
vector<Vec4i> hierarchy;



//
//
//-(id)init {
//    if ( self = [super init] ) {
//        
//    }
//    return self;
//}





void init(){

//    camera = [[CvVideoCamera alloc] initWithParentView: _imageView];
//    camera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
//    camera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
//    camera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
//    camera.defaultFPS = 30;
//    camera.grayscaleMode = NO;
//    camera.delegate = self;
}

//-----------------------------------------------------Squares.cpp Algo--------------------------------------------------------



-(NSNumber * ) findSquaresContours: (cv::Mat) imageMat : (vector<cv::Point> &) square {
    
    NSNumber *  rectangleSize = [self findSquares:imageMat :square ];

    return rectangleSize;
}

static double angle( cv::Point pt1, cv::Point pt2, cv::Point pt0 )
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}
//// returns sequence of squares detected on the image.
//// the sequence is stored in the specified memory storage
-(NSNumber * ) findSquares : (const Mat&) image : (vector<cv::Point> &) square
{
    
    Mat pyr, timg, gray0(image.size(), CV_8U), gray;

    // down-scale and upscale the image to filter out the noise
    pyrDown(image, pyr, cv::Size(image.cols/2, image.rows/2));
    pyrUp(pyr, timg, image.size());
    vector<vector<cv::Point> > squareContours;
    
    
    double curBiggest = 0;

    // find squares in every color plane of the image
    for( int c = 0; c < 3; c++ )
    {
        int ch[] = {c, 0};
        mixChannels(&timg, 1, &gray0, 1, ch, 1);

        // try several threshold levels
        for( int l = 0; l < N; l++ )
        {
            if( 0 )
            {
                // apply Canny. Take the upper threshold from slider
                // and set the lower to 0 (which forces edges merging)
                Canny(gray0, gray, 0, squaresThresh, 5);
                // dilate canny output to remove potential
                // holes between edge segments
                dilate(gray, gray, Mat(), cv::Point(-1,-1));
            }
            else
            {
                // apply threshold if l!=0:
                //     tgray(x,y) = gray(x,y) < (l+1)*255/N ? 255 : 0
                gray = gray0 >= (l+1)*255/N;
            }
            
            // find contours and store them all as a list
            findContours(gray, squareContours, RETR_LIST, CHAIN_APPROX_SIMPLE);

            vector<cv::Point> approx;

            // test each contour
            for( size_t i = 0; i < squareContours.size(); i++ )
            {
                // approximate contour with accuracy proportional
                // to the contour perimeter
                approxPolyDP(Mat(squareContours[i]), approx, arcLength(Mat(squareContours[i]), true)*0.02, true);

                // square contours should have 4 vertices after approximation
                // relatively large area (to filter out noisy contours)
                // and be convex.
                // Note: absolute value of an area is used because
                // area may be positive or negative - in accordance with the
                // contour orientation
                double relativeSquareSize = fabs(contourArea(Mat(approx))/(src.rows * src.cols));
                
                
                
                if( approx.size() == 4 &&
                   relativeSquareSize > lowerThreshHoldSize  && relativeSquareSize < upperThreshHoldSize &&
                   isContourConvex(Mat(approx))
                   )
                {
                    cout << relativeSquareSize<< endl;
                    double maxCosine = 0;

                    for( int j = 2; j < 5; j++ )
                    {
                        // find the maximum cosine of the angle between joint edges
                        double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                        maxCosine = MAX(maxCosine, cosine);
                    }

                    // if cosines of all angles are small
                    // (all angles are ~90 degree) then write quandrange
                    // vertices to resultant sequence
                    if( maxCosine < 0.2 ){
                        if(relativeSquareSize> curBiggest) {
                            curBiggest = relativeSquareSize;
                            cout << relativeSquareSize << endl;
                            cout <<  "Pusing back square size: " << relativeSquareSize << endl;
                            square = approx;
                        }

                     //   return [NSNumber numberWithDouble:relativeSquareSize];
                    }
                    
                }
            }
        }
    }
    return [NSNumber numberWithDouble:curBiggest];
    //return NULL;
}
//// the function draws all the squares in the image
static void drawSquares( Mat& image, const vector<cv::Point>& square )
{
        const cv::Point* p = &square[0];
        int n = (int)square.size();
        polylines(image, &p, &n, 1, true, Scalar(255,0,0), 15, LINE_AA);
    
}
//-----------------------------------------------------//Squares.cpp Algo--------------------------------------------------------



//-----------------------------------------------------Edge Analysis Algo--------------------------------------------------------



double discardLinesAndFindLargest( vector< vector<cv::Point> >  & contours, vector<cv::Point> & largestContour ){
    
    vector<vector<cv::Point> >::iterator it;
    
    double largestContourArea = 0 ;
    
    for(it = contours.begin();  it != contours.end(); ){
        float sizeLimit = (src.cols * src.rows)/(100/minPercentContour);
        double area =contourArea(*it);
        if(area>=sizeLimit){
            if(area > largestContourArea){
                largestContourArea=area;
                largestContour = *it;
            }
            
        }
        ++it;
    }
    return largestContourArea;
}

void Erosion()
{
    bitwise_not(canny_output,canny_output);
    int erosion_type;
    if( erosion_elem == 0 ){ erosion_type = MORPH_RECT; }
    else if( erosion_elem == 1 ){ erosion_type = MORPH_CROSS; }
    else if( erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }
    
    Mat element = getStructuringElement( erosion_type,
                                        cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                        cv::Point( erosion_size, erosion_size ) );
    
    /// Apply the erosion operation
    erode( canny_output, canny_output, element );
    bitwise_not(canny_output,canny_output);
    
}

void Prep(){
    cvtColor( src, src_gray, CV_BGR2GRAY );
    blur( src_gray, src_gray, cv::Size(4,4) );
}
void DrawContoursToImage(Mat & drawing){
    /// Draw contours
    
    for( int i = 0; i< contours.size(); i++ )
    {
        //Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours( drawing, contours, i, Scalar(0,255,0), 15, 8, hierarchy, 0, cv::Point() );
    }

    
}

- (UIImage * ) analysis: (UIImage *) image : ( double * ) result {
    
    src = [self cvMatFromUIImage:image];
    
    
    vector<cv::Point>  square;
    
    NSNumber * squareSizeNS =[self findSquaresContours:src : square ] ;
    if([squareSizeNS isEqual:@0])
        return NULL;
    

    
    float squareSize = [squareSizeNS floatValue ];
    
        /// Convert image to gray and blur it
    Prep();
    

    
    /// Detect edges using canny
    Canny( src_gray, canny_output, thresh, thresh*2, 3 );

    Erosion();
    

    //ShowImage(canny_output);
    /// Find contours
    findContours( canny_output, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
    //    findContours(src, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    vector <cv::Point> largestContour;
    double biggestContour = discardLinesAndFindLargest(contours, largestContour);
    
    contours.clear();
    contours.push_back(largestContour);
    
    DrawContoursToImage(src);
    
    
    drawSquares(src, square);
    

    double realArea = (squareSizePaper/double(squareSize))*(double)(biggestContour/(src.rows* src.cols));
    
    *result = realArea;
    
    //return [self UIImageFromCVMat:src];
    return [self UIImageFromCVMat:src];
}

//-----------------------------------------------------//Edge Analysis Algo--------------------------------------------------------
- (cv::Mat)cvMatFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}
-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat
{
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                        cvMat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * cvMat.elemSize(),                       //bits per pixel
                                        cvMat.step[0],                            //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    return finalImage;
}

-(UIImage * )uniformScale:(UIImage*) image
{
    cv::Mat cvMat = [self cvMatFromUIImage:image];
    cv::Size s = cvMat.size();
    int rows = s.height;
    int cols = s.width;
    
    cv::Size size(150,cols /(rows/150));//the dst image size,e.g.100x100
    Mat dst;//dst image
    resize(cvMat,dst,size);//resize image
    
    return [self UIImageFromCVMat:dst];
}





@end
