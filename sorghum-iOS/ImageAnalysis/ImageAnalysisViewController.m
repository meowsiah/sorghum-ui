//
//  ImageAnalysisViewController.m
//  sorghum-iOS
//
//  Created by cis on 11/22/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import "ImageAnalysisViewController.h"
#import "ImageAnalysis.hpp"

@interface ImageAnalysisViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *squareImage;
@property (strong,nonatomic) UIImage * oldImage;
//- (IBAction)slider:(UISlider *)sender;

@property (weak, nonatomic) IBOutlet UITextField *inputField;

- (IBAction)go:(UIButton *)sender;


@end


@implementation ImageAnalysisViewController

-(void) viewDidLoad{
    [super viewDidLoad];
    _imageAnalyzer = [[ImageAnalysis alloc] init];
    _oldImage = _squareImage.image;
}


- (IBAction)go:(UIButton *)sender {
    //_squareImage.image = [[self imageAnalyzer] squareFinderHough:_oldImage :_slider.value];
//    _squareImage.image = [[self imageAnalyzer] findSquaresContours:_oldImage :_inputField.text.intValue] ;
}




@end
