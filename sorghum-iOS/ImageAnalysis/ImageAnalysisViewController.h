//
//  ImageAnalysisViewController.h
//  sorghum-iOS
//
//  Created by cis on 11/22/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardFriendlyViewController.h"
@class ImageAnalysis;

@interface ImageAnalysisViewController : KeyboardFriendlyViewController

@property (strong, nonatomic) ImageAnalysis * imageAnalyzer;

@end
