//
//  PhotoCollectionViewController.m
//  sorghum-iOS
//
//  Created by cis on 11/25/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//
/*
#import "PhotoCollectionViewController.h"
#import "Cell.h"
#import "FinalScreen.h"

@interface PhotoCollectionViewController ()

@end


@implementation PhotoCollectionViewController


static NSString * const reuseIdentifier = @"Cell";

//- (id)init
//{
//    return [super initWithNibName:@"photoCollectionIdentifier" bundle:nil];
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - UICollectionViewDataSource Protocol methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.imagePHAssets count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Cell *cell = (Cell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCellIdentifier" forIndexPath:indexPath];
    
    UIImage * phImageAsset = [self.imagePHAssets objectAtIndex:indexPath.row];
    if(phImageAsset == NULL){
        NSLog(@"noimageASSET");
    }
    UIImage * imageAsset = phImageAsset;
    cell.imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
    [cell.contentView addSubview:cell.imageView];
    
    //cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    cell.imageView.image= imageAsset;
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout Protocol methods
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
//{
//    return CGSizeMake(100, 100);
//}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section;
//{
//    return UIEdgeInsetsMake(0, 0, 0, 0);
//}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"dataPass"])
    {
        // Get reference to the destination view controller
         FinalScreen *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setFinalAverage:5.0f];
    }
}


//- (UIImage*)grabImageFromAsset:(PHAsset *)asset
//{
//    __block UIImage *returnImage;
//    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
//    options.synchronous = YES;
//    [[PHImageManager defaultManager] requestImageForAsset:asset
//                                               targetSize:CGSizeMake(asset.pixelWidth, asset.pixelHeight)
//                                              contentMode:PHImageContentModeAspectFill
//                                                  options:options
//                                            resultHandler:
//     ^(UIImage *result, NSDictionary *info) {
//         returnImage = result;
//     }];
//    return returnImage;
//}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
