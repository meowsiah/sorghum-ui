//
//  Demo.m
//  sorghum-iOS
//
//  Created by cis on 4/11/16.
//  Copyright © 2016 Michael Bradshaw. All rights reserved.
//

#import "Demo.h"
#include "ImageAnalysis.hpp"
#import "FinalScreen.h"

@implementation Demo
@synthesize result;


static NSString * InvalidImageError = @"Image does not contain square!";

static NSString * DuplicateImageError  = @"You have already selected this image";

static int MinImageCount = 1;

//static NSString *messages[] = {
//    @"1: Original",
//    @"2: Turn into black/white and blur to filter noise",
//    @"3: Apply Canny Edge Detector",
//    @"4: Erode white points -> a continous contour",
//    @"5: Find countours",
//    @"6: Filter out small contours",
//    @"7: Find Square and highlight it"
//    };


-(void) viewDidLoad{
    [super viewDidLoad];
    _imageAnalyzer = [[ImageAnalysis alloc] init];
    _results = [[NSMutableArray alloc] init];
    _originalImages =[[NSMutableArray alloc] init];
}

- (IBAction)takePhoto:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (IBAction)selectPhoto:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    UIImage *chosenImage = [self rotate:info[UIImagePickerControllerOriginalImage] andOrientation:UIImageOrientationUp ];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.center = self.view.center;
    indicator.hidesWhenStopped = YES;
    indicator.color = [UIColor blackColor];
    [self.view addSubview:indicator];
    [indicator startAnimating];
    [self hideAllUI];
    
    [picker dismissViewControllerAnimated:YES completion: ^{[self handleSelectedImage:chosenImage withDictionary:info andImagePicker:(UIImagePickerController *) picker withLoaderIndicator:indicator];}];
    
   }

- (void) handleSelectedImage:(UIImage *)chosenImage  withDictionary:(NSDictionary *)info andImagePicker:(UIImagePickerController *) picker withLoaderIndicator:(UIActivityIndicatorView *) indicator {
    
    
    _imageToDisplay =NULL;

    
    if(picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary){
        for( NSURL * curImageString in _originalImages){
            if([curImageString isEqual:info[UIImagePickerControllerReferenceURL]]){
                [_areaText setText:DuplicateImageError];
                [indicator stopAnimating];
                [self presentImagePickingDialog];
                _imageToDisplay=NULL;
                return;
            }
        }
        
    }
    
    result = 0;
    _imageToDisplay=[[self imageAnalyzer] analysis:chosenImage :&result] ;
    
    if(_imageToDisplay == NULL){
        [_areaText setText:InvalidImageError];
        [indicator stopAnimating];
        [self presentImagePickingDialog];
        return;
    }
    
    _curImageURL =info[UIImagePickerControllerReferenceURL];
    
    
    
    [self presentImageAcceptDialog];
    [indicator stopAnimating];

}
- (void) displayCurrentImage{
       
    self.imageView.image= _imageToDisplay;
    NSString * toSet = [NSString stringWithFormat:@"Area of the head is : %.2lf inches²", result];
        [[self areaText] setText: toSet ];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



-(UIImage*) rotate:(UIImage*) src andOrientation:(UIImageOrientation)orientation
{
    UIGraphicsBeginImageContext(src.size);
    
    CGContextRef context=(UIGraphicsGetCurrentContext());
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, 90/180*M_PI) ;
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, -90/180*M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 90/180*M_PI);
    }
    
    [src drawAtPoint:CGPointMake(0, 0)];
    UIImage *img=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"finalSegue"]) {
        
        // Get destination view
        FinalScreen * vc = [segue destinationViewController];
        
        double finalAverage = 0;
        for(NSNumber * curNumber in _results){
            finalAverage += [curNumber doubleValue];
        }
        finalAverage/= _results.count;
        
        [vc setAppAreaAverage:[NSNumber numberWithDouble:finalAverage]];
        
        
    }
}
- (void) presentImagePickingDialog{
    _photoButton.hidden = false;
    _selectButton.hidden = false;
    _imageView.image = NULL;
    
    _acceptImageButton.hidden = true;
    _dismissImageButton.hidden = true;
    
    _yieldDisplayTransitionButton.hidden = false;
    if(_results.count >= MinImageCount)
        _progressBar.hidden=false;
    if(_imageToDisplay!=NULL )
        _areaText.hidden = true;
    else
        _areaText.hidden = false;
    
}

- (void) presentImageAcceptDialog{
    _photoButton.hidden = true;
    _selectButton.hidden = true;
    [self displayCurrentImage];
    
    _acceptImageButton.hidden = false;
    _dismissImageButton.hidden = false;
    
    _yieldDisplayTransitionButton.hidden = true;
    _progressBar.hidden=true;
    
    _areaText.hidden = false;
}

- (void) hideAllUI{
    
    _photoButton.hidden = true;
    _selectButton.hidden = true;
    _imageView.image = NULL;
    
    _acceptImageButton.hidden = true;
    _dismissImageButton.hidden = true;
    
    _yieldDisplayTransitionButton.hidden = true;
    _progressBar.hidden =true;
    
    _areaText.hidden =true;
    
}

- (IBAction)acceptImage:(UIButton *)sender {
    [_results addObject:[NSNumber numberWithDouble:result]];
    [self presentImagePickingDialog];
    _yieldDisplayTransitionButton.hidden = false;
    _progressBar.progress = (float)_results.count/10;
    if(_curImageURL!=NULL){
        [_originalImages addObject:_curImageURL];
        _curImageURL=NULL;
    }
}

- (IBAction)dismissImage:(UIButton *)sender {
    [self presentImagePickingDialog];
}

@end
