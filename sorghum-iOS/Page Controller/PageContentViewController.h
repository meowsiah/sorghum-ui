//
//  PageContentViewController.h
//  sorghum-iOS
//
//  Created by cis on 4/18/16.
//  Copyright © 2016 Michael Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;

@end
