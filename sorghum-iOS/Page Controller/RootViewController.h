//
//  RootViewController.h
//  sorghum-iOS
//
//  Created by cis on 4/18/16.
//  Copyright © 2016 Michael Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface RootViewController : UIViewController
- (IBAction)startWalkThrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@end
