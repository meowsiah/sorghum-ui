//
//  Demo.h
//  sorghum-iOS
//
//  Created by cis on 4/11/16.
//  Copyright © 2016 Michael Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageAnalysis;

@interface Demo : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)takePhoto:(UIButton *)sender;

- (IBAction)selectPhoto:(UIButton *)sender;

@property (nonatomic,strong) ImageAnalysis * imageAnalyzer;
@property (strong, nonatomic) IBOutlet UILabel *areaText;


@property (strong, nonatomic) UIImage * imageToDisplay;

@property double  result;
@property NSMutableArray * results;

- (void) displayCurrentImage;

- (void) presentImagePickingDialog;

- (void) presentImageAcceptDialog;

- (void) hideAllUI;

- (void) handleSelectedImage:(UIImage *)chosenImage  withDictionary:(NSDictionary *)info andImagePicker:(UIImagePickerController *) picker withLoaderIndicator:(UIActivityIndicatorView *) indicator;

-(UIImage*) rotate:(UIImage*) src andOrientation:(UIImageOrientation)orientation;
@property (strong, nonatomic) IBOutlet UIButton *photoButton;
@property (strong, nonatomic) IBOutlet UIButton *selectButton;
@property (strong, nonatomic) IBOutlet UIButton *yieldDisplayTransitionButton;


- (IBAction)acceptImage:(UIButton *)sender;
- (IBAction)dismissImage:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *acceptImageButton;
@property (strong, nonatomic) IBOutlet UIButton *dismissImageButton;

@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;



@property NSMutableArray * originalImages;

@property NSURL * curImageURL;

//@property (strong, nonatomic) CvVideoCamera *camera;



@end
