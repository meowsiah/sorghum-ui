 //
//  main.m
//  sorghum-iOS
//
//  Created by Michael Bradshaw on 10/7/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
