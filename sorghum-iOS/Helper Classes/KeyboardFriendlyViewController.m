//
//  KeyboardFriendlyViewController.m
//  sorghum-iOS
//
//  Created by K-State CIS on 10/16/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

//This class was made so that any subclass will close keyboard on background clicks
#import "KeyboardFriendlyViewController.h"

@interface KeyboardFriendlyViewController ()

@end

@implementation KeyboardFriendlyViewController

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:YES];
}

//On load a UITapGestureRecognizer is added to the view. The above method handleSingleTap then registers a tap on the background and will remove the keyboard if it is up
- (void)viewDidLoad {
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
