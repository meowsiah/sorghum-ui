//
//  Constants.h
//  sorghum-iOS
//
//  Created by K-State CIS on 11/4/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#include <stdio.h>

//url paths
extern const char URL[];
extern const char API_V1[];
extern const char CREATE_USER[];
extern const char LOGIN[];
extern const char RESET[];
extern const char REPORT[];

//Keys for NSUserDefaults

//FieldSize
//RowSpacing
//HeadsPerAcre
//FieldName
//access_token
//refresh_token
//user

//Example to get data from defaults
//NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//NSLog(@"access token: %@", [defaults objectForKey: @"access_token"]);

//error messages
extern char* const ERRORS[7];

//method to set the error messages
#if defined __cplusplus
extern "C" {
#endif
    
    void setConstants();
    
#if defined __cplusplus
};
#endif

#endif /* Constants_h */
