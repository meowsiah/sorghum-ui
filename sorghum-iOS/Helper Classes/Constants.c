//
//  Constants.m
//  sorghum-iOS
//
//  Created by K-State CIS on 11/4/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#include "Constants.h"

//url paths
const char API_V1[] = "/api/v1";
const char URL[] = "http://sorghum.cis.ksu.edu";
const char CREATE_USER[] = "/user/";
const char LOGIN[] = "/auth/";
const char RESET[] = "/reset/";
const char REPORT[] = "/report/";


//error messages
extern char* const ERRORS[] = {"Please fill out all the boxes",
    "Username is taken",
    "Username does not exist",
    "Incorrect password" ,
    "Invalid email address",
    "Email is taken",
    "Please fill out all fields correctly"};
