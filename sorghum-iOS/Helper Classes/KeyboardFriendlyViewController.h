//
//  KeyboardFriendlyViewController.h
//  sorghum-iOS
//
//  Created by K-State CIS on 10/16/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

//This class was made so that any subclass will close keyboard on background clicks

#import <UIKit/UIKit.h>

@interface KeyboardFriendlyViewController : UIViewController <UITextFieldDelegate>

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
@end
