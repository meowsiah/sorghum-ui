//
//  LoginViewController.h
//  sorghum-iOS
//
//  Created by K-State CIS on 11/2/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import "KeyboardFriendlyViewController.h"
#import "Constants.h"


@interface LoginViewController : KeyboardFriendlyViewController  

@property (strong, nonatomic) IBOutlet UITextField *UsernameTxt;
@property (strong, nonatomic) IBOutlet UITextField *PasswordTxt;

-(IBAction)Login:(id)sender;
@end
