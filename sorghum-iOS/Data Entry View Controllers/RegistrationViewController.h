//
//  ViewController.h
//  sorghum-iOS
//
//  Created by Michael Bradshaw on 10/7/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import "KeyboardFriendlyViewController.h"
#import "Constants.h"

@interface RegistrationViewController : KeyboardFriendlyViewController <NSURLConnectionDelegate>


@end

