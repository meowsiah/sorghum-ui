//
//  DataScreenViewController.h
//  sorghum-iOS
//
//  Created by K-State CIS on 10/12/15.
//  Copyright © 2015 Kansas State University. All rights reserved.
//

#import "KeyboardFriendlyViewController.h"
#import "Constants.h"

@interface DataScreenViewController : KeyboardFriendlyViewController <UITextFieldDelegate>
{
    int FieldSize;
    int HeadsPerThousandthAcre;
    float RowSpacing;
    int FieldSizeStepperOldValue;
    int HeadsPerAcreStepperOldValue;
}

@property (strong, nonatomic) IBOutlet UITextField *FieldNameTxt;
@property (strong, nonatomic) IBOutlet UITextField *FieldSizeTxt;
@property (strong, nonatomic) IBOutlet UITextField *HeadsPerAcreTxt;
@property (strong, nonatomic) IBOutlet UIScrollView *Scroll;
@property (weak, nonatomic) IBOutlet UIStepper *FieldSizeStepper;
@property (weak, nonatomic) IBOutlet UIStepper *HeadsPerAcreStepper;

//The stepper elements
-(IBAction)FieldSizeStepperAction:(id)sender;
-(IBAction)HeadsPerAcreStepperAction:(id)sender;

//The row spacing buttons
-(IBAction)RowSpacing7point5:(id)sender;
-(IBAction)RowSpacing15:(id)sender;
-(IBAction)RowSpacing30:(id)sender;

-(IBAction)MeasureSheetYes:(id)sender;
-(IBAction)MeasureSheetNo:(id)sender;

//Hook this up to a tap gesture recognizer to register a click on the background
//-(IBAction)BackgroundClick:(id)sender;

@end
