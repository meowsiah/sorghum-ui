//
//  FinalScreen.h
//  sorghum-iOS
//
//  Created by cis on 1/26/16.
//  Copyright © 2016 Michael Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface FinalScreen : UIViewController <CLLocationManagerDelegate>


@property (weak, nonatomic) IBOutlet UILabel *averageDisplay;
@property (weak, nonatomic) IBOutlet UILabel *yieldDisplay;
@property (weak, nonatomic) IBOutlet UILabel *sliderCaption;

@property (strong,nonatomic) NSString* baseText;

- (IBAction)Soil:(id)sender;
- (IBAction)Weather:(id)sender;
- (IBAction)sizeSlider:(id)sender;
@property (weak, nonatomic) IBOutlet UISlider *sliderValue;

@property (strong, nonatomic) NSNumber * appAreaAverage;
@property (strong, nonatomic) NSNumber * grainCount;
@property (strong, nonatomic) NSNumber * rowSpacing;
@property (strong, nonatomic) NSNumber * numberOfHeadsCounted;
@property (strong, nonatomic) NSNumber * seedsPerPound;

- (void)setParameters:(NSNumber * ) appAreaAveragePass;

- (void)displayYield;

@end
