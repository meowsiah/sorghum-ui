//
//  FinalScreen.m
//  sorghum-iOS
//
//  Created by cis on 1/26/16.
//  Copyright © 2016 Michael Bradshaw. All rights reserved.
//

#import "FinalScreen.h"

@implementation FinalScreen
{
    CLLocationManager *locationManager;
    NSUserDefaults *defaults;
}

@synthesize averageDisplay;
@synthesize yieldDisplay;
@synthesize appAreaAverage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    defaults = [NSUserDefaults standardUserDefaults];
    locationManager = [[CLLocationManager alloc] init];
    [self->locationManager requestWhenInUseAuthorization];
    [self getCurrentLocation];
    
    int toSet = [self areaToGrainCount:appAreaAverage.floatValue];
    _grainCount = [NSNumber numberWithInt:toSet];
    [averageDisplay     setText: [NSString stringWithFormat:@"%.2lf", appAreaAverage.doubleValue]];
    
    
    self.seedsPerPound = [NSNumber numberWithFloat:_sliderValue.value];
    
   // [yieldDisplay     setText: [NSString stringWithFormat:@"%f", (double)(_grainCount.intValue * (1.0f/self.seedsPerPound.intValue) ) ]];
    [self displayYield];
    _baseText = _sliderCaption.text;
    
    //_sliderCaption.text = [_baseText stringByAppendingString:[[NSNumber numberWithFloat:_sliderValue.value] stringValue]];
    NSString * string1 = _baseText;
    NSString * string2 = [NSString stringWithFormat:@" - %d", (int)_sliderValue.value] ;
    
    NSString * toPrint = [string1 stringByAppendingString:string2];
    [_sliderCaption setText:toPrint];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

//Starts tracking location. This is set up to stop updating after finding location once
- (void)getCurrentLocation {
    NSLog(@"get");
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        //set long and lat and stop updating location
        
        [locationManager stopUpdatingLocation];
        [defaults setDouble: currentLocation.coordinate.latitude forKey:@"Latitude"];
        [defaults setDouble: currentLocation.coordinate.longitude forKey:@"Longitude"];
    }
}

- (void)displayYield{
    
    [yieldDisplay     setText: [NSString stringWithFormat:@"%f bushels/head", (double)(_grainCount.intValue * (1.0f/self.seedsPerPound.intValue) ) ]];

}

- (int)areaToGrainCount:(float)area{
    
    return (int)((113.6 * area) + 255) ;
}
- (double) returnYield{

    return (double)(_grainCount.intValue * (1.0f/self.seedsPerPound.intValue) );
}


- (IBAction)sizeSlider:(id)sender {
    
   // _sliderCaption.text = [_baseText stringByAppendingString:[[NSNumber numberWithFloat:_sliderValue.value] stringValue]];
    
    self.seedsPerPound = [NSNumber numberWithFloat:_sliderValue.value];
    
    
    [self displayYield];
    
    
    NSString * string1 = _baseText;
    NSString * string2 = [NSString stringWithFormat:@" - %d", (int)_sliderValue.value] ;
    
    NSString * toPrint = [string1 stringByAppendingString:string2];

    
    [_sliderCaption setText:toPrint];
    
    
}
- (void)setParameters:(NSNumber * ) appAreaAveragePass{
   // [self appAreaAverage] = appAreaAveragePass;
}

- (IBAction)Soil:(id)sender{
    UIWebView *webView = [[UIWebView alloc] init];
    [webView setFrame:CGRectMake(0, 0, 320, 460)];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]]; //find soil conditions based on gps location
    [[self view] addSubview:webView];
}
- (IBAction)Weather:(id)sender{
    UIWebView *webView = [[UIWebView alloc] init];
    [webView setFrame:CGRectMake(0, 0, 320, 460)];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]]; //find soil conditions based on gps location
    [[self view] addSubview:webView];
}

@end
