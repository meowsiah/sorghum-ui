//
//  PasswordResetViewController.h
//  sorghum-iOS
//
//  Created by K-State CIS on 10/30/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import "KeyboardFriendlyViewController.h"
#import "Constants.h"

@interface PasswordResetViewController : KeyboardFriendlyViewController

@property (strong, nonatomic) IBOutlet UITextField *UsernameTxt;
@property (strong, nonatomic) IBOutlet UITextField *EmailTxt;
@property (strong, nonatomic) IBOutlet UILabel *test;
-(IBAction)Submit:(id)sender;

@end
