//
//  PasswordResetViewController.m
//  sorghum-iOS
//
//  Created by K-State CIS on 10/30/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import "PasswordResetViewController.h"

@interface PasswordResetViewController ()

@end

@implementation PasswordResetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)Submit:(id)sender{
    
    NSString* path = [NSString stringWithFormat:@"%s%s%s", URL, API_V1, RESET];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[[NSString stringWithFormat: @"username=%@", _EmailTxt.text]dataUsingEncoding:NSUTF8StringEncoding]];
    
    //create a connectin with the server with a delegate method to handle completion
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
          NSInteger responseStatusCode = [httpResponse statusCode];
          if (responseStatusCode == 500)
          {
              //internal server error. display a message about it
          }
          else if (responseStatusCode == 400)
          {
              //Bad request. display a message about it
          }
          else if (responseStatusCode == 404)
          {
              //Page not found. display a message about it
          }
          else if (data && responseStatusCode == 201) //success
          {
              //turn the response data into json, then transfer the json into a dictonary
              
              NSError* error2 = nil;
              NSError* error3;
              NSData* jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error2];
              NSLog(@"%@", jsonResponse);
              NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error3];
              
              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your password has been reset" preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {}];
              [alert addAction:defaultAction];
              
              //display success message
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self presentViewController:alert animated:YES completion:nil];
              });
              
              //create an aleart with the error
              //                  alert.message = [NSString stringWithFormat:@"Reset failed: %@", errorMessage];
              //                  dispatch_async(dispatch_get_main_queue(), ^{
              //                      [self presentViewController:alert animated:YES completion:nil];
              //                  });
          }
      }] resume];

    
    //handle url session error
    
  
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
