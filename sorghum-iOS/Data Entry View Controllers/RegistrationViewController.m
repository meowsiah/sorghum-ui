//
//  RegistrationViewController.m
//  sorghum-iOS
//
//  Created by Michael Bradshaw on 10/7/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import "RegistrationViewController.h"

@interface RegistrationViewController ()


- (IBAction)SubmitButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *userPassword;
@property (weak, nonatomic) IBOutlet UITextField *repeatPassword;
@property (weak, nonatomic) IBOutlet UITextField *email;

@end

@implementation RegistrationViewController


- (IBAction)SubmitButton:(id)sender {
    if([self CheckEmail: _email.text]){
        
    }
    
    if (![self CheckForText]){
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message: [NSString stringWithFormat:@"%s", ERRORS[0]]preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    
    if([self CheckPassWord] == false){
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message: @"Passwords do not match"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    
    NSString* path = [NSString stringWithFormat:@"%s%s%s", URL, API_V1, CREATE_USER];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path]];
    [urlRequest setHTTPMethod:@"POST"];
    
    //JSON conversion and reading test
    
    //NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:@"Chris", @"Name", @"Handyside", @"Last", @"tree.gov", @"Email",  nil];
    //NSError *error;
    //NSData* postData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
    //NSString *string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    //NSLog(@"JSON: %@", [string dataUsingEncoding:NSUTF8StringEncoding]);
    //[urlRequest setHTTPBody:[string dataUsingEncoding:NSUTF8StringEncoding]];
    
    //Country was newly added and still needs a text box with a list of valid countries
    [urlRequest setHTTPBody:[[NSString stringWithFormat:@"first_name=%@&last_name=%@&email=%@&password=%@&country=US", _firstName.text, _lastName.text, _email.text, _userPassword.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //create a connectin with the server with a delegate method to handle completion
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          //Code for logging data, request, and error.
          
          //NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
          //NSLog(@"data: %@", data);
          //NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
          //NSLog(@"Data = %@",text);
          NSLog(@"response: %@", response);
          NSLog(@"error: %@", error);
          
          //get status code and respond to them depending on the code
          
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
          NSInteger responseStatusCode = [httpResponse statusCode];
          if (responseStatusCode == 500)
          {
              //internal server error. display a message about it
              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"500 Internal server error" preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {}];
              [alert addAction:defaultAction];
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self presentViewController:alert animated:YES completion:nil];
              });
          }
          
          else if (responseStatusCode == 400)
          {
              //Page not found. display a message about it
              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message:[NSString stringWithFormat:@"%s", ERRORS[6]] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {}];
              [alert addAction:defaultAction];
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self presentViewController:alert animated:YES completion:nil];
              });
          }
          
          else if (responseStatusCode == 404)
          {
              //Page not found. display a message about it
              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"404 Page not found" preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {}];
              [alert addAction:defaultAction];
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self presentViewController:alert animated:YES completion:nil];
              });
          }
          
          else if (data && responseStatusCode == 201)
          {
              //turn the response data into json, then transfer the json into a dictonary
              NSError* error2 = nil;
              NSData* jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error2];
              NSLog(@"JSON DATA SERIALIZED: %@", jsonResponse);
              
              NSError* error3;
              NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error3];
              
              //User successfully created. display a message about it
              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"New user created" preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {}];
              [alert addAction:defaultAction];
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self presentViewController:alert animated:YES completion:nil];
              });
          }
          
      }] resume];
    
    //handle errors with session
    
}

- (BOOL)CheckForText{
    //check that each field has text in it
    if ([_email.text isEqual:(@"")])
    {
        return false;
    }
    if ([_firstName.text isEqual:(@"")])
    {
        return false;
    }
    if ([_lastName.text isEqual:(@"")])
    {
        return false;
    }
    if ([_userPassword.text isEqual:(@"")])
    {
        return false;
    }
    if ([_repeatPassword.text isEqual:(@"")])
    {
        return false;
    }
    return true;
}

- (BOOL)CheckEmail:(NSString *)userNameString {
    //check for valid email address
    return false;
}
- (BOOL)CheckPassWord {
    //check if passwords match
    if (_userPassword.text==_repeatPassword.text) {
        return true;
    }
    else{
        return false;
    }
    
    return false;
}

//makes the keyboard "next" key go to the next text field
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _email) {
        [textField resignFirstResponder];
        [_firstName becomeFirstResponder];
    }
    else if (textField == _firstName) {
        [textField resignFirstResponder];
        [_lastName becomeFirstResponder];
    }
    //lastName and on dont set the next textbox correctly. This could be because the keyboard covers the textbox it is trying to go to.
    else if (textField == _lastName) {
        [textField resignFirstResponder];
        [_userPassword becomeFirstResponder];
    }
    else if (textField == _userPassword) {
        [textField resignFirstResponder];
        [_repeatPassword becomeFirstResponder];
    }
    else if (textField == _repeatPassword) {
        [textField resignFirstResponder];
    }
    return YES;
}
@end
