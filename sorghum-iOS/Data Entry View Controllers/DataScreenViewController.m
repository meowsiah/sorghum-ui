//
//  DataScreenViewController.m
//  sorghum-iOS
//
//  Created by K-State CIS on 10/12/15.
//  Copyright © 2015 Kansas State University. All rights reserved.
//

#import "DataScreenViewController.h"

@interface DataScreenViewController ()

@end

@implementation DataScreenViewController

@synthesize FieldNameTxt, HeadsPerAcreTxt, FieldSizeTxt, HeadsPerAcreStepper, FieldSizeStepper;

-(IBAction)RowSpacing7point5:(id)sender
{
    RowSpacing = 7.5f;
}

-(IBAction)RowSpacing15:(id)sender
{
    RowSpacing = 15;
}

-(IBAction)RowSpacing30:(id)sender
{
    RowSpacing = 30;
}

//the stepper actions first do a check to see if it was a + or - click by comparing to the old value. Then add or subtract to the value in the correct text field
-(IBAction)HeadsPerAcreStepperAction:(id)sender
{
    int plusMinus = 0;
    if ((int)HeadsPerAcreStepper.value > HeadsPerAcreStepperOldValue)
    { plusMinus = 1;}
    else
    { plusMinus = -1;}
    HeadsPerAcreStepperOldValue += HeadsPerAcreStepper.stepValue * plusMinus;
    int temp = [HeadsPerAcreTxt.text intValue];
    temp += self.HeadsPerAcreStepper.stepValue * plusMinus;
    
    //No negative numbers from hitting - on the stepper too many times
    if (temp >= 0)
    {
        HeadsPerAcreTxt.text = [NSString stringWithFormat:@"%d", temp];
    }
    else
    {
        HeadsPerAcreStepper.value = 0;
        HeadsPerAcreStepperOldValue = 0;
        HeadsPerAcreTxt.text = [NSString stringWithFormat:@"%d", 0];
    }
}

-(IBAction)FieldSizeStepperAction:(id)sender
{
    int plusMinus = 0;
    if ((int)FieldSizeStepper.value > FieldSizeStepperOldValue)
    { plusMinus = 1;}
    else
    { plusMinus = -1;}
    FieldSizeStepperOldValue += FieldSizeStepper.stepValue * plusMinus;
    int temp = [FieldSizeTxt.text intValue];
    temp += self.FieldSizeStepper.stepValue * plusMinus;
    FieldSizeTxt.text = [NSString stringWithFormat:@"%d", temp];
    
    //No negative numbers from hitting - on the stepper too many times
    if (temp >= 0)
    {
        FieldSizeTxt.text = [NSString stringWithFormat:@"%d", temp];
    }
    else
    {
        FieldSizeStepper.value = 0;
        FieldSizeStepperOldValue = 0;
        FieldSizeTxt.text = [NSString stringWithFormat:@"%d", 0];
    }
}

//All values on the Data Screen will be saved to NSUserDefaults so that they will not have to be entered again if the app is closed after this point
-(IBAction)MeasureSheetYes:(id)sender
{
    
    if ([FieldSizeTxt.text intValue] <= 0 || [HeadsPerAcreTxt.text intValue] <=0)
    {
        NSLog(@"ERRORS: %s", ERRORS[0]);
        //Show error about not filling out all boxes correctly
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message:[NSString stringWithFormat:@"%s", ERRORS[0]] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    else
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setInteger:[FieldSizeTxt.text intValue] forKey:@"FieldSize"];
        [defaults setFloat:RowSpacing forKey:@"RowSpacing"];
        [defaults setInteger:[HeadsPerAcreTxt.text intValue] forKey:@"HeadsPerAcre"];
        [defaults setObject:FieldNameTxt.text forKey:@"FieldName"];
        
        NSLog(@"%@", [defaults objectForKey:@"FieldSize"]);
        NSLog(@"%@", [defaults objectForKey:@"RowSpacing"]);
        
        //Check authentication?
        //Go to image view
    }
}

-(IBAction)MeasureSheetNo:(id)sender
{
    //Take them to a download link or send it to their email
}

- (void)viewDidLoad {
    [super viewDidLoad];
    RowSpacing = 0;
    HeadsPerAcreStepperOldValue = (int)HeadsPerAcreStepper.value;
    FieldSizeStepperOldValue = (int)FieldSizeStepper.value;
    _Scroll.contentSize = CGSizeMake(900, 600);
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
