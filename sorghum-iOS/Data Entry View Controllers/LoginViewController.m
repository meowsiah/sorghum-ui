//
//  LoginViewController.m
//  sorghum-iOS
//
//  Created by K-State CIS on 11/2/15.
//  Copyright © 2015 Michael Bradshaw. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
{
    BOOL shouldSeague;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Check to see the last user and their access / refresh tokens
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //A log to see if who the last user was and their tokens
    NSLog(@"access token: %@", [defaults objectForKey: @"access_token"]);
    NSLog(@"refresh token: %@", [defaults objectForKey: @"refresh_token"]);
    NSLog(@"user %@", [defaults objectForKey:@"user"]);
    
    if ([defaults objectForKey:@"access_token"] != nil)
    {
        //send acess token to server, if response is bad send refresh token to server. If both responses are bad set user defaults to null and force a relog.
        
        //if resopnse was good then enable a "Continue" button that will reload all the data and images they had already input or put them back at the report screen if they ran a report
        
        //There was also mention that if the access token / refresh token are still good, skip this screen and add a logout button in the later screens. This way the user will be automatically logged in ever time the app starts
    }
    
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    
    UIImageView * profileIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"profile"]];
    profileIcon.frame = CGRectMake(0.0, 0.0, profileIcon.image.size.width+10.0, profileIcon.image.size.height);
    profileIcon.contentMode = UIViewContentModeCenter;
    
    _UsernameTxt.leftViewMode =UITextFieldViewModeAlways;
    _UsernameTxt.leftView = profileIcon;
    
    UIImageView * lockIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lock"]];
    lockIcon.frame = CGRectMake(0.0, 0.0, lockIcon.image.size.width+10.0, lockIcon.image.size.height);
    lockIcon.contentMode = UIViewContentModeCenter;
    
    _PasswordTxt.leftViewMode = UITextFieldViewModeAlways;
    _PasswordTxt.leftView = lockIcon;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)Login:(id)sender{
    
    shouldSeague = false;
    
    NSString* path = [NSString stringWithFormat:@"%s%s%s", URL, API_V1, LOGIN];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[[NSString stringWithFormat: @"email=%@&password=%@", _UsernameTxt.text, _PasswordTxt.text]dataUsingEncoding:NSUTF8StringEncoding]];
    
    //create a connectin with the server with a delegate method to handle completion
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          //get status code and deal with it. 200 = sucess and fufilled. 500 = internal server error. might need more cases for other errors (404, 201, 202, etc)
          
          NSLog(@"%@", response);
          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
          NSInteger responseStatusCode = [httpResponse statusCode];
          
          if (responseStatusCode == 500)
          {
              //internal server error. display a message about it
              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"500 Internal server error" preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {}];
              [alert addAction:defaultAction];
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self presentViewController:alert animated:YES completion:nil];
              });
          }
          
          else if (responseStatusCode == 400)
          {
              //Page not found. display a message about it
              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message:[NSString stringWithFormat:@"%s", ERRORS[0]] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {}];
              [alert addAction:defaultAction];
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self presentViewController:alert animated:YES completion:nil];
              });
          }
          
          else if (responseStatusCode == 403)
          {
              //Page not found. display a message about it
              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message:[NSString stringWithFormat:@"%s", ERRORS[6]] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {}];
              [alert addAction:defaultAction];
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self presentViewController:alert animated:YES completion:nil];
              });
          }
          
          else if (responseStatusCode == 404)
          {
              //Page not found. display a message about it
              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"404 Page not found" preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {}];
              [alert addAction:defaultAction];
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self presentViewController:alert animated:YES completion:nil];
              });
          }
          
          else if (data && responseStatusCode == 201) //success
          {
              //turn the response data into json, then transfer the json into a dictonary
              
              NSError* error2 = nil;
              NSError* error3;
              NSData* jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error2];
              NSLog(@"JSON DATA: %@", jsonResponse);
              NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error3];
              
              //Save token data
              NSDictionary *tokenDictionary = [responseDictionary objectForKey:@"tokens"];
              NSString *access_token = [tokenDictionary objectForKey:@"access_token"];
              NSString *refresh_token = [tokenDictionary objectForKey:@"refresh_token"];
              //NSInteger expires_in = [[tokenDictionary objectForKey:@"expires_in"] intValue];
              
              NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
              
              [defaults setObject:access_token forKey:@"access_token"];
              [defaults setObject:refresh_token forKey:@"refresh_token"];
              //[defaults setInteger:expires_in forKey:@"expires_in"];
              [defaults setObject:_UsernameTxt.text forKey:@"user"];
              
              //log data
              //NSLog(@"access token: %@", [defaults objectForKey: @"access_token"]);
              //NSLog(@"refresh token: %@", [defaults objectForKey: @"refresh_token"]);
              //NSLog(@"user %@", [defaults objectForKey:@"user"]);
              
              //login as the user and procede to data screen
              
              shouldSeague = TRUE;
              

          }
      }] resume];
    NSLog(@"SHOULD SEAGUE: %@", shouldSeague);
    if (shouldSeague == TRUE)
    {
        [self performSegueWithIdentifier:@"login_to_data" sender:self];
    }
    //handel url session errors
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
