//
//  QBImagePicker.h
//  QBImagePicker
//
//  Created by Katsuma Tanaka on 2015/04/03.
//  Copyright (c) 2015 Katsuma Tanaka. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for QBImagePicker.
FOUNDATION_EXPORT double QBImagePickerVersionNumber;

//! Project version string for QBImagePicker.
FOUNDATION_EXPORT const unsigned char QBImagePickerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <QBImagePicker/PublicHeader.h>
#import "QBImagePickerController.h"
//#import <QBImagePicker/QBImagePickerController.h>


//Copyright (c) 2015 Katsuma Tanaka

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
